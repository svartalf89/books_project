<?php

namespace App\Exporter\ExporterManager;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use App\Facades\Exporter;
use App\Exporter\ProcessorInterface;
use App\Models\Book;

class ExporterTest extends TestCase
{
    public function testsInitializeNewProcessor()
    {
        $processor = Exporter::initializeNewExportProcessor('csv', 1);
        $this->assertInstanceOf(ProcessorInterface::class, $processor);
    }

    public function testsCsvProcessorGetFileId()
    {
        $processor = Exporter::initializeNewExportProcessor('csv', 1);
        $this->assertTrue(method_exists($processor, 'getFileId'));
        $this->assertEquals('export_1.csv', $processor->getFileId());
    }

    public function testsXmlProcessorGetFileId()
    {
        $processor = Exporter::initializeNewExportProcessor('xml', 1);
        $this->assertTrue(method_exists($processor, 'getFileId'));
        $this->assertEquals('export_1.xml', $processor->getFileId());
    }

    public function testsCsvProcessorWriteInChunks()
    {
        Storage::fake('export');
        $driver = Storage::disk('export');
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $mock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['callback'])
            ->getMock();

        $mock->expects($this->once())
            ->method('callback')
            ->with($this->equalTo(1), $this->equalTo(1));

        $processor = Exporter::initializeNewExportProcessor('csv', 1);
        $processor->writeInChunks($book, ['author', 'title'], [$mock, 'callback']);

        $fileContents = $driver->get('export_1.csv');
        $this->assertEquals('author,title' . PHP_EOL.
            '"First Author","First Book"' . PHP_EOL, $fileContents);
    }

    public function testsXmlProcessorWriteInChunks()
    {
        Storage::fake('export');
        $driver = Storage::disk('export');
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $mock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['callback'])
            ->getMock();

        $mock->expects($this->once())
            ->method('callback')
            ->with($this->equalTo(1), $this->equalTo(1));

        $processor = Exporter::initializeNewExportProcessor('xml', 1);
        $processor->writeInChunks($book, ['author', 'title'], [$mock, 'callback']);

        $fileContents = $driver->get('export_1.xml');
        $this->assertXmlStringEqualsXmlString('<?xml version="1.0"?>' .
            '<root>' .
            '<Book>' .
            '  <author>First Author</author>' .
            '  <title>First Book</title>' .
            '</Book>' .
            '</root>', $fileContents);
    }

    public function testsGetDownloadStreamAndMetadata()
    {
        Storage::fake('export');
        Storage::disk('export')->put('fake_1.xml', '<root></root>');

        list($stream, $metadata) = Exporter::getDownloadStreamAndMetadata('fake_1.xml');

        $this->assertArraySubset([
            'mime'  => 'text/plain',
            'filename' => 'fake_1.xml'
        ], $metadata);
        $this->assertTrue(is_resource($stream));
    }

    public function testsDeleteFileIfExists()
    {
        Storage::fake('export');
        $driver = Storage::disk('export');
        $driver->put('fake_1.xml', '<root></root>');

        $this->assertTrue($driver->exists('fake_1.xml'));
        Exporter::deleteFileIfExists('fake_1.xml');
        $this->assertFalse($driver->exists('fake_1.xml'));
    }
}
