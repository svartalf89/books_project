<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Facades\QueryModifier;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class QueryModifierTest extends TestCase
{
    public function testsApplyPreCountQueryModifiers()
    {
        $request = new Request();
        $request->setMethod('GET');
        $request->replace(['search' => 'some term']);

        $builder = $this->getMockBuilder(EloquentBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(['where', 'orWhere'])
            ->getMock();

        $builder->method('where')->will($this->returnSelf());
        $builder->method('orWhere')->will($this->returnSelf());

        $builder->expects($this->once())
            ->method('where');
        $builder->expects($this->once())
            ->method('orWhere');

        $query = QueryModifier::applyPreCountQueryModifiers($request, $builder, 'Book');
    }

    public function testsApplyPostCountQueryModifiers()
    {
        $request = new Request();
        $request->setMethod('GET');
        $request->replace(['sort' => 'author', 'order' => 'desc', 'start' => 1, 'limit' => 100]);

        $builder = $this->getMockBuilder(EloquentBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(['orderBy', 'offset', 'limit'])
            ->getMock();

        $builder->method('orderBy')->will($this->returnSelf());
        $builder->method('offset')->will($this->returnSelf());
        $builder->method('limit')->will($this->returnSelf());

        $builder->expects($this->once())
            ->method('orderBy');
        $builder->expects($this->once())
            ->method('offset');
        $builder->expects($this->once())
            ->method('limit');

        $query = QueryModifier::applyPostCountQueryModifiers($request, $builder, 'Book');
    }
}
