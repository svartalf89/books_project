<?php

namespace Tests\Feature;

use Tests\TestCase;
use Imtigger\LaravelJobStatus\JobStatus;
use Illuminate\Support\Facades\Storage;

class ExportTest extends TestCase
{
    public function _testsExportStart()
    {
        $payload =  [
            'format' => 'csv',
            'columns' => 'author,title'
        ];

        $this->json('GET', '/api/export', $payload)
            ->assertStatus(200)
            ->assertJsonStructure(['jobId']);
    }

    public function testsExportGetStatus()
    {
        $jobStatus = factory(JobStatus::class)->create([
            'job_id' => 1,
        ]);

        $this->json('GET', '/api/export/1')
            ->assertStatus(200)
            ->assertJsonStructure(['status', 'progressPercentage']);
    }

    public function testsExportFinished()
    {
        $jobStatus = factory(JobStatus::class)->create([
            'job_id' => 1,
            'status' => JobStatus::STATUS_FINISHED,
            'progress_now' => 10,
            'progress_max' => 10,
            'output' => ['fileId' => 'test_1.csv']
        ]);

        $this->json('GET', '/api/export/1')
            ->assertStatus(200)
            ->assertExactJson([
                'status' => 'finished',
                'progressPercentage' => 100,
                'fileId' => 'test_1.csv'
            ]);
    }

    public function testsExportDownload()
    {
        Storage::fake('export');
        Storage::disk('export')->put('fake_1.csv', 'title, author');

        $this->json('GET', '/api/export/download/fake_1.csv')
            ->assertHeader('Pragma', 'public')
            ->assertHeader('Cache-Control')
            ->assertHeader('Content-Type', 'text/plain; charset=UTF-8')
            ->assertHeader('Content-disposition', 'attachment; filename="fake_1.csv"');
    }
}
