<?php

use Tests\TestCase;
use App\Models\Book;

class BookTest extends TestCase
{
    public function _testsBooksAreCreatedCorrectly()
    {
        $payload = [
            'title' => 'Lorem Ipsum',
            'author' => 'John Doe',
        ];

        $this->json('POST', '/api/books', $payload)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'title' => 'Lorem Ipsum', 'author' => 'John Doe']);
    }

    public function testsBooksAreUpdatedCorrectly()
    {
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $payload = [
            'title' => 'Lorem Ipsum',
            'author' => 'John Doe',
        ];

        $response = $this->json('PUT', '/api/books/' . $book->id, $payload)
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'title' => 'First Book',
                'author' => 'John Doe',
            ]);
    }

    public function testsBooksAreDeletedCorrectly()
    {
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $this->json('DELETE', '/api/books/' . $book->id, [])
            ->assertStatus(204);
    }

    public function testBooksAreListedCorrectly()
    {
        $books = [
            factory(Book::class)->create([
                'title' => 'First Book',
                'author' => 'First Author'
            ]),
            factory(Book::class)->create([
                'title' => 'Second Book',
                'author' => 'Second Author'
            ])
        ];

        // generic use-case
        $this->json('GET', '/api/books', ['limit' => '10'])
            ->assertStatus(200)
            ->assertExactJson([
                'total' => 2,
                'items' => [
                    [ 'id' => $books[0]->id, 'title' => 'First Book', 'author' => 'First Author' ],
                    [ 'id' => $books[1]->id, 'title' => 'Second Book', 'author' => 'Second Author' ]
                ]
            ]);

        // with search
        $this->json('GET', '/api/books', ['limit' => '10', 'search' => 'Second'])
            ->assertStatus(200)
            ->assertExactJson([
                'total' => 1,
                'items' => [
                    [ 'id' => $books[1]->id, 'title' => 'Second Book', 'author' => 'Second Author' ]
                ]
            ]);

        // with sort and order
        $this->json('GET', '/api/books', ['limit' => '10', 'sort' => 'title', 'order' => 'desc'])
            ->assertStatus(200)
            ->assertExactJson([
                'total' => 2,
                'items' => [
                    [ 'id' => $books[1]->id, 'title' => 'Second Book', 'author' => 'Second Author' ],
                    [ 'id' => $books[0]->id, 'title' => 'First Book', 'author' => 'First Author' ]
                ]
            ]);
    }

}