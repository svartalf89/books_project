<?php

namespace App\Exporter;

use Illuminate\Contracts\Filesystem\Filesystem;
use App\Exporter\InvalidExportFormatException;
use App\Exporter\Processors\Csv as CsvExporter;
use App\Exporter\Processors\Xml as XmlExporter;

class ExporterManager
{
    /**
     * Predefined XML format
     *
     * @var string
     */
    const XML = 'xml';

    /**
     * Predefined csv format
     *
     * @var string
     */
    const CSV = 'csv';

    /**
     * Laravel Files Storage
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $storage;

    /**
     * Mapping between types and classes
     *
     * @var array
     */
    protected $processors = [
        self::CSV => CsvExporter::class,
        self::XML => XmlExporter::class,
    ];

    /**
     * Constructor, initializes storage instance
     *
     * @param \Illuminate\Contracts\Filesystem\Filesystem $storage
     * @return void
     */
    public function __construct(Filesystem $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Generates uniq file id, based on job id currently
     *
     * @param string $format
     * @param string $id
     * @return string
     */
    protected function generateFileId($format, $id)
    {
        return 'export_' . $id . '.' . $format;
    }

    /**
     * Instantiates proper processor
     *
     * @param string $format
     * @param string $id
     * @return \App\Exporter\ProcessorInterface
     */
    private function createProcessor($format, $id)
    {
        $fileId = $this->generateFileId($format, $id);
        return new $this->processors[$format]($fileId, $this->storage->path($fileId));
    }

    /**
     * Public API - resolves and creates new \App\Exporter\ProcessorInterface processor
     *
     * @param string $format
     * @param string $id
     * @return \App\Exporter\ProcessorInterface
     * @throws \App\Exporter\InvalidExportFormatException;
     */
    public function initializeNewExportProcessor($format, $id)
    {
        if (!in_array($format, [static::XML, static::CSV])) {
            throw new InvalidExportFormatException('Invalid export format.');
        }

        return $this->createProcessor($format, $id);
    }

    /**
     * Public API - creates stream as well as metadata for streamed download response
     *
     * @param string $fileId
     * @return array
     */
    public function getDownloadStreamAndMetadata($fileId)
    {
        $driver = $this->storage->getDriver();

        return [
            $driver->readStream($fileId),
            [
                'mime'  => $driver->getMimetype($fileId),
                'filename' => $fileId
            ]
        ];
    }

    /**
     * Public API - deletes file from defined storage
     *
     * @param string $fileId
     * @return boolean
     */
    public function deleteFileIfExists($fileId)
    {
        return $this->storage->delete($fileId);
    }
}
