<?php

namespace App\Exporter\Processors;

use App\Exporter\ProcessorInterface;

class Csv implements ProcessorInterface
{
    /**
     * File id to return on demand.
     *
     * @var string
     */
    protected $fileId;

    /**
     * Raw file path, to write on disk.
     *
     * @var string
     */
    protected $fileRawPath;

    /**
     * Constructor, set protected vars
     *
     * @param string $fileId
     * @param string $fileRawPath
     * @return void
     */
    public function __construct($fileId, $fileRawPath)
    {
        $this->fileId = $fileId;
        $this->fileRawPath = $fileRawPath;
    }

    /**
     * Returns fileId for further usage.
     *
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Perform write to CSV file in chunks.
     * Will call callback on each iteration.
     *
     * @param string $model Class name of Eloquent model
     * @param array $columns
     * @param \Closure $callable Callback
     * @param integer $chunkSize
     * @return string
     */
    public function writeInChunks($model, array $columns, callable $callable, $chunkSize = 500)
    {
        $modelInstance = $model::select($columns);
        $total = $modelInstance->count();
        $totalChunks = (int) ceil($total / $chunkSize);

        $this->startWrite($columns);

        $modelInstance->chunk($chunkSize, function($rows, $i) use ($callable, $totalChunks, $model) {
            $this->writeChunk($rows->toArray(), class_basename($model));
            call_user_func_array($callable, [$i, $totalChunks]);
        });

        return $this->fileId;
    }

    /**
     * Creates file and writes headings
     *
     * @param array $columns
     * @return void
     */
    private function startWrite($columns)
    {
        $resource = fopen($this->fileRawPath, 'w');
        fputcsv($resource, $columns);
        fclose($resource);
    }

    /**
     * Appends single chunk into file
     *
     * @param array $chunk
     * @return void
     */
    private function writeChunk($chunk)
    {
        $resource = fopen($this->fileRawPath, 'a');
        foreach($chunk as $i => $row) {
            fputcsv($resource, $row);
        }
        fclose($resource);
    }
}
