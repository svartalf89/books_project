<?php

namespace App\Exporter\Processors;

use App\Exporter\ProcessorInterface;

class Xml implements ProcessorInterface
{
    /**
     * File id to return on demand.
     *
     * @var string
     */
    protected $fileId;

    /**
     * Raw file path, to write on disk.
     *
     * @var string
     */
    protected $fileRawPath;

    /**
     * Constructor, set protected vars
     *
     * @param string $fileId
     * @param string $fileRawPath
     * @return void
     */
    public function __construct($fileId, $fileRawPath)
    {
        $this->fileId = $fileId;
        $this->fileRawPath = $fileRawPath;
    }

    /**
     * Returns fileId for further usage.
     *
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Perform write to CSV file in chunks.
     * Will call callback on each iteration.
     *
     * @param string $model Class name of Eloquent model
     * @param array $columns
     * @param \Closure $callable Callback
     * @param integer $chunkSize
     * @return string
     */
    public function writeInChunks($model, array $columns, callable $callable, $chunkSize = 500)
    {
        $modelInstance = $model::select($columns);
        $total = $modelInstance->count();
        $totalChunks = (int) ceil($total / $chunkSize);

        $this->startWrite();

        $modelInstance->chunk($chunkSize, function($rows, $i) use ($callable, $totalChunks, $model) {
            $this->writeChunk($rows->toArray(), class_basename($model));
            call_user_func_array($callable, [$i, $totalChunks]);
        });

        $this->endWrite();

        return $this->fileId;
    }

    /**
     * Creates file and writes <?xml version and root element
     *
     * @return void
     */
    private function startWrite()
    {
        $str = '<?xml version="1.0"?>' . PHP_EOL;
        $str .='<root>' . PHP_EOL;

        $resource = fopen($this->fileRawPath, 'w');
        fwrite($resource, $str);
        fclose($resource);
    }

    /**
     * Appends single chunk into file
     *
     * @param array $chunk
     * @param string $wrapper
     * @return void
     */
    private function writeChunk($chunk, $wrapper)
    {
        $str = '';

        foreach($chunk as $i => $row) {
            $xml = new \xmlWriter();
            $xml->openMemory();

            $xml->setIndent(true);
            $xml->setIndentString('  ');

            $xml->startElement($wrapper);

            foreach ($row as $field => $value) {
                $xml->startElement($field);
                $xml->text($value);
                $xml->endElement();
            }

            $xml->endElement();

            $str .= trim($xml->outputMemory(true)) . PHP_EOL;
        }

        $resource = fopen($this->fileRawPath, 'a');
        fwrite($resource, $str);
        fclose($resource);
    }

    /**
     * Finalizes xml file, closes root tag
     *
     * @return void
     */
    private function endWrite()
    {
        $str ='</root>';

        $resource = fopen($this->fileRawPath, 'a');
        fwrite($resource, $str);
        fclose($resource);
    }
}
