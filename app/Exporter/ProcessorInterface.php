<?php
namespace App\Exporter;

interface ProcessorInterface {
    /**
     * Should return fileId for further usage.
     *
     * @return string
     */
    public function getFileId();

    /**
     * Should perform write to file in chunks(internally).
     * And call callback on each iteration.
     *
     * @param string $model
     * @param array $columns
     * @param \Closure $callable
     * @param integer $chunkSize
     * @return string
     */
    public function writeInChunks($model, array $columns, callable $callable, $chunkSize);
}
