<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * Columns that can be mass-assigned
     *
     * @var array
     */
    protected $fillable = ['title', 'author'];

    /**
     * Columns visible when converting to array
     *
     * @var array
     */
    protected $visible = ['id', 'title', 'author'];
}
