<?php

namespace App\Http\Traits;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Imtigger\LaravelJobStatus\JobStatus;
use App\Jobs\ExportJob;
use App\Facades\Exporter;

trait Exportable
{
    /**
     * Dispatches new job into Queue
     *
     * @param string $model Eloquent Model class name
     * @param array $columns exportable columns
     * @param string $format
     * @return array
     */
    protected function dispatchExportJob($model, $columns, $format)
    {
        $job = new ExportJob($model, $columns, $format);
        dispatch($job);

        return [
            'jobId' => $job->getJobStatusId()
        ];
    }

    /**
     * Retrieves already dispatched job by its jobId
     *
     * @param string $jobId
     * @return array
     */
    protected function getDispatchedExportJob($jobId)
    {
        $jobStatus = JobStatus::find($jobId);

        if(empty($jobStatus)) {
            throw new NotFoundHttpException('Job not found');
        }

        return array_merge([
            'status' => $jobStatus->status,
            'progressPercentage' => $jobStatus->progressPercentage
        ], $jobStatus->output ?? []);
    }

    /**
     * Generates download stream and headers
     *
     * @param string $fileId
     * @return array
     */
    protected function downloadExportStream($fileId)
    {
        list($stream, $metadata) = Exporter::getDownloadStreamAndMetadata($fileId);

        return response()->stream(
            function() use ($stream) {
                while(ob_get_level() > 0) ob_end_flush();
                fpassthru($stream);
            },
            200,
            [
                'Pragma' => 'public',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Type' => $metadata['mime'],
                'Content-disposition' => 'attachment; filename="' . $metadata['filename'] . '"',
            ]
        );
    }
}
