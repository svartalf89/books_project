<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Facades\QueryModifier;

trait Listable
{
    /**
     * Applies all query modifiers to query and returns result array
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $ns sub-folder inside QueryModifiers directory
     * @return array
     */
    protected function getSearchableList(Request $request, Builder $query, $ns)
    {
        $query = QueryModifier::applyPreCountQueryModifiers($request, $query, $ns);
        $count = $query->count();
        $query = QueryModifier::applyPostCountQueryModifiers($request, $query, $ns);

        return [
            'total' => $count,
            'items' => $query->get()
        ];
    }
}
