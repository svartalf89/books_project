<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Traits\Exportable;
use App\Http\Requests\Export\{
    Books as BooksRequest
};

class ExportController extends Controller
{
    use Exportable;

    /**
     * Export collection of Book model
     *
     * @param \App\Http\Requests\Export\Books
     * @return \Illuminate\Http\Response
     */
    public function books(BooksRequest $request)
    {

        $resp = $this->dispatchExportJob(Book::class,
            $request->input('columns'),
            $request->input('format')
        );

        return response()->json($resp, 200);
    }

    /**
     * Get current Job status by jobId
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function getJob(Request $request, $jobId)
    {
        $resp = $this->getDispatchedExportJob($jobId);

        return response()->json($resp, 200);
    }

    /**
     * Generates streamable download with appropriate headers
     *
     * @param \Illuminate\Http\Request
     * @param string $fileId
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request, $fileId)
    {
        return $this->downloadExportStream($fileId);
    }
}
