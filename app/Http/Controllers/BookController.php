<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Traits\Listable;
use App\Http\Requests\Book\{
    Create as CreateRequest,
    Update as UpdateRequest,
    Index as IndexRequest
};

class BookController extends Controller
{
    use Listable;

    /**
     * Returns searchable, sortable and limitable list of books
     *
     * @param \App\Http\Requests\Book\Index
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $resp = $this->getSearchableList($request, Book::select(), 'Book');

        return response()->json($resp, 200);
    }

    /**
     * Creates new book model and stores it into DB
     *
     * @param \App\Http\Requests\Book\Create
     * @return \Illuminate\Http\Response
     */
    public function create(CreateRequest $request)
    {
        $book = Book::create($request->validated());

        return response()->json($book, 201);
    }

    /**
     * Updates booksm model and stores it into DB
     *
     * @param \App\Http\Requests\Book\Update
     * @param \App\Models\Book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Book $book)
    {
        $book->update($request->validated());

        return response()->json($book, 200);
    }

    /**
     * Deletes Book from DB
     *
     * @param \App\Models\Book
     * @return \Illuminate\Http\Response
     */
    public function delete(Book $book)
    {
        $book->delete();

        return response()->json(null, 204);
    }
}
