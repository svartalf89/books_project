<?php

namespace App\Http\Requests\Book;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $maxLimit = Config::get('settings.listable.maxLimit');

        return [
            'limit' => 'required|integer|min:1|max:' . $maxLimit,
            'search' => 'string',
            'sort' => 'string|in:author,title',
            'order' => 'string|in:asc,desc',
            'start' => 'integer|min:1'
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'The :attribute field is missing.',
            'integer' => 'Invalid :attribute value.',
            'min' => 'Invalid :attribute value.',
            'max' => 'Maximum amount exceeded. Maximum :max per request.',
            'string' => 'Invalid :attribute value.',
            'in' => 'Invalid :attribute value.'
        ];
    }
}
