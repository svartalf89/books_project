<?php

namespace App\Http\Requests\Export;

use Illuminate\Foundation\Http\FormRequest;

class Books extends FormRequest
{
    /**
     * Preprocess request params before validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'columns' => $this->has('columns')
                ? explode(',', $this->input('columns'))
                : ['author', 'title']
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'columns' => 'array',
            'columns.*' => 'string|in:author,title',
            'format' => 'required|string|in:csv,xml'
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'The :attribute field is missing.',
            'string' => 'Invalid :attribute value.',
            'in' => 'Invalid :attribute value.',
            'columns.*.in' => 'Invalid columns value.'
        ];
    }
}
