<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Imtigger\LaravelJobStatus\Trackable;
use App\Facades\Exporter;

class ExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    /**
     * Eloquent Model class name
     *
     * @var string
     */
    protected $model;

    /**
     * Exportable columns
     *
     * @var array
     */
    protected $columns;

    /**
     * Export format
     *
     * @var string
     */
    protected $format;

    /**
     * Number of re-tries
     *
     * @var integer
     */
    public $tries = 1;

    /**
     * Timeout in seconds
     *
     * @var integer
     */
    public $timeout = 300;

    /**
     * Create a new job instance.
     *
     * @param string $model Eloquent Model class name
     * @param array $columns
     * @param string $format
     */
    public function __construct($model, array $columns, $format)
    {
        $this->prepareStatus();
        $this->model = $model;
        $this->columns = $columns;
        $this->format = $format;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $processor = Exporter::initializeNewExportProcessor($this->format,
            $this->getJobStatusId()
        );
        $fileId = $processor->getFileId();

        $this->setInput([$fileId, $this->model, $this->columns, $this->format]);

        $processor->writeInChunks($this->model, $this->columns, function($current, $total) {
            if (!$this->progressMax) {
                $this->setProgressMax($total);
            }
            $this->setProgressNow($current);
        }, Config::get('settings.export.chunkSize'));

        $this->setOutput(['fileId' => $fileId]);
    }
}
