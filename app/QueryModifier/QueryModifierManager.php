<?php

namespace App\QueryModifier;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class QueryModifierManager
{
    /**
     * Type pre count
     *
     * @var string
     */
    const PRE_COUNT_TYPE = 'pre_count';

    /**
     * Type post count
     *
     * @var string
     */
    const POST_COUNT_TYPE = 'post_count';

    /**
     * Resolves modifier class
     *
     * @param string $modifier
     * @param string $ns
     * @return string
     */
    private static function generateModifierClass($modifier, $ns)
    {
        return __NAMESPACE__ . '\\' . $ns . '\\' . studly_case($modifier);
    }

    /**
     * Applies query modifiers with specific type
     *
     * @param string $type
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $ns sub-folder inside QueryModifiers directory
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function applyQueryModifiersWithType($type, Request $request, Builder $query, $ns)
    {
        foreach ($request->all() as $modifier => $value) {
            $modifierClass = static::generateModifierClass($modifier, $ns);

            if (class_exists($modifierClass) && $modifierClass::TYPE === $type) {
                $query = $modifierClass::apply($request, $query);
            }
        }

        return $query;
    }

    /**
     * Applies pre count query modifiers based on request params
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $ns sub-folder inside QueryModifiers directory
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function applyPreCountQueryModifiers(Request $request, Builder $query, $ns)
    {
        return $this->applyQueryModifiersWithType(static::PRE_COUNT_TYPE, $request, $query, $ns);
    }

    /**
     * Applies post count query modifiers based on request params
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $ns sub-folder inside QueryModifiers directory
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function applyPostCountQueryModifiers(Request $request, Builder $query, $ns)
    {
        return $this->applyQueryModifiersWithType(static::POST_COUNT_TYPE, $request, $query, $ns);
    }
}
