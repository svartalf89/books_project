<?php
namespace App\QueryModifier;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

interface ModifierInterface
{
    /**
     * Should apply all required custom logic to query
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder for chaining
     */
    public static function apply(Request $request, Builder $query);
}