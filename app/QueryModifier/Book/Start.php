<?php

namespace App\QueryModifier\Book;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\QueryModifier\ModifierInterface;

class Start implements ModifierInterface
{
    /**
     * Type
     *
     * @var string
     */
    const TYPE = 'post_count';

    /**
     * Applies start(offset) from request to query
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function apply(Request $request, Builder $query)
    {
        if ($request->has('start')) {
            $query->offset($request->input('start') - 1);
        }

        return $query;
    }
}