<?php

namespace App\QueryModifier\Book;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\QueryModifier\ModifierInterface;

class Sort implements ModifierInterface
{
    /**
     * Type
     *
     * @var string
     */
    const TYPE = 'post_count';

    /**
     * Default order if not provided in request
     *
     * @pvar string
     */
    private static $defaultOrder = 'asc';

    /**
     * Applies sort and order from request to query
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function apply(Request $request, Builder $query)
    {
        if ($request->has('sort')) {
            $query->orderBy($request->input('sort'),
                $request->input('order') ?? static::$defaultOrder);
        }

        return $query;
    }
}