<?php

namespace App\QueryModifier\Book;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\QueryModifier\ModifierInterface;

class Search implements ModifierInterface
{
    /**
     * Type
     *
     * @var string
     */
    const TYPE = 'pre_count';

    /**
     * Applies search from request to query
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function apply(Request $request, Builder $query)
    {
        if ($request->has('search')) {
            $query
                ->where('title', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('author', 'LIKE', '%' . $request->input('search') . '%');
        }

        return $query;
    }
}