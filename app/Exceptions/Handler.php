<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use League\Flysystem\FileNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($request->is('api/*')) {
            switch(true) {
                case $exception instanceof FileNotFoundException:
                    $err = ['error' => 'File not found'];
                    $code = 404;
                    break;
                case $exception instanceof NotFoundHttpException:
                    $err = ['error' => 'Route not found'];
                    $code = 404;
                    break;
                case $exception instanceof ValidationException:
                    $err = ['errors' => $exception->errors()];
                    $code = 400;
                    break;
                default:
                    $err = ['error' => $exception->getMessage()];
                    $code = 400;
            }

            return response()->json($err, $code);
        } else {
            return parent::render($request, $exception);
        }
    }
}
