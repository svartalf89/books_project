<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Imtigger\LaravelJobStatus\JobStatus;
use App\Facades\Exporter;
use App\Jobs\ExportJob;


class CleanUpExpiredExports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean-up expired export jobs and files';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expire = Config::get('settings.export.expireMinutes');
        $query = JobStatus::where('type', ExportJob::class)
            ->where('finished_at', '<', Carbon::now()->subMinutes($expire)->toDateTimeString());

        foreach($query->get() as $row) {
            if (!empty($row->input)) {
                Exporter::deleteFileIfExists($row->input[0]);
            }
        }

        $query->delete();
    }
}
