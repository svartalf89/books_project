<?php

use Faker\Generator as Faker;
use Imtigger\LaravelJobStatus\JobStatus;
use App\Jobs\ExportJob;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(JobStatus::class, function (Faker $faker) {
    return [
        'job_id' => $faker->unique()->randomDigit,
        'type' => ExportJob::class,
        'queue' => 'default',
        'progress_now' => rand(1, 10),
        'progress_max' => rand(1, 10),
        'status' => $faker->randomElement([JobStatus::STATUS_QUEUED, JobStatus::STATUS_EXECUTING,
            JobStatus::STATUS_FINISHED, JobStatus::STATUS_FAILED]),
    ];
});
