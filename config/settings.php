<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Books list settings
    |--------------------------------------------------------------------------
    |
    | As it says, contains various settings for books list, e.g. maxLimit
    |
    */

    'listable' => [
        'maxLimit' => 1000
    ],

    /*
    |--------------------------------------------------------------------------
    | Export settings
    |--------------------------------------------------------------------------
    |
    | Export settings, e.g. chunk size for $model->chunk() for export
    |
    */

    'export' => [
        'chunkSize' => 500,
        'expireMinutes' => 30
    ]

];
