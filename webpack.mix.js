const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

// Karma doesn't work with mix.extract properly :(
if (process.env.NODE_ENV !== 'testing') {
  mix.extract(['underscore', 'angular', 'angular-aria', 'angular-material', 'angular-animate', 'angular-messages',
    'angular-ui-router']);
}

mix.disableNotifications();

if (mix.inProduction()) {
  mix.version();
}
