import 'angular';
import 'angular-mocks';
import './app'

// require all test files using special Webpack feature
// https://webpack.github.io/docs/context.html#require-context
const specsContext = require.context(".", true, /\.spec$/);
specsContext.keys().forEach(specsContext);