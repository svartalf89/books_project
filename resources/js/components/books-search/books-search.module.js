import angular from 'angular';
import { BooksSearchComponent } from './books-search.component';

export const BooksSearchModule = angular
  .module('books-search', [])
  .component('booksSearch', BooksSearchComponent)
  .name;