describe('BooksSearchModule', () => {
  let controller;
  let SharedEventsBusService;
  let SHARED_EVENTS;

  beforeEach(() => {
    angular.mock.module('app');
  });

  beforeEach(inject(($componentController, _SharedEventsBusService_, _SHARED_EVENTS_) => {
    SharedEventsBusService = _SharedEventsBusService_;
    SHARED_EVENTS = _SHARED_EVENTS_;

    controller = $componentController('booksSearch');
  }));

  it('should be defined', () => {
    expect(controller).toBeDefined()
  });

  it('should dispatch search event', () => {
    spyOn(SharedEventsBusService, 'dispatch');

    controller.searchTerm = 'search term';
    controller.runSearch();

    expect(SharedEventsBusService.dispatch).toHaveBeenCalledWith(SHARED_EVENTS.booksSearch, 'search term');
  })
});