import template from './books-search.html';

class BooksSearchController {
  constructor(SHARED_EVENTS, SharedEventsBusService) {
    this.SHARED_EVENTS = SHARED_EVENTS;
    this.SharedEventsBusService = SharedEventsBusService;
  }

  runSearch() {
    this.SharedEventsBusService.dispatch(this.SHARED_EVENTS.booksSearch, this.searchTerm);
  }
}

BooksSearchController.$inject = ['SHARED_EVENTS', 'SharedEventsBusService'];

export const BooksSearchComponent = {
  template,
  controller: BooksSearchController
};
