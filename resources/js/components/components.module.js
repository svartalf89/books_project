import angular from 'angular';
import { MainLayoutModule } from './main-layout/main-layout.module';
import { BooksListModule } from './books-list/books-list.module';
import { BooksSearchModule } from './books-search/books-search.module';
import { BooksAddModule } from './books-add/books-add.module';
import { ExportModule } from './export/export.module';

export const ComponentsModule = angular
  .module('app.components', [
    MainLayoutModule,
    BooksListModule,
    BooksSearchModule,
    BooksAddModule,
    ExportModule
  ])
  .name;
