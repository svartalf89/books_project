import template from './book-item.html';

class BookItemController {
  constructor(SHARED_EVENTS, SharedEventsBusService, BookItemService) {
    this.SHARED_EVENTS = SHARED_EVENTS;
    this.SharedEventsBusService = SharedEventsBusService;
    this.BookItemService = BookItemService;
  }

  $onInit() {
    this.state = 'display';
  }

  _setIsLocked(locked) {
    this._isActionsLocked = locked;
  }

  _checkIsLocked() {
    return !!this._isActionsLocked;
  }

  toggleState(state) {
    if (state === this.state) return;

    switch (state) {
      case 'display':
        this.editItem = null;
        break;
      case 'edit':
        this.editItem = _.extend({}, this.item);
        break;
    }
    this.state = state;
  }

  updateItem(fields) {
    if (!this.editItem) return;

    this.BookItemService.requestBookUpdate(this.item.id, _.pick(this.editItem, fields)).then((data) => {
      this.item = _.extend({}, this.editItem);
      this.toggleState('display');
    });
  }

  deleteItem() {
    if (this._checkIsLocked()) return;

    this._setIsLocked(true);
    this.BookItemService.requestBookDelete(this.item.id).then(() => {
      this.SharedEventsBusService.dispatch(this.SHARED_EVENTS.booksRerender);
    }).finally(() => {
      this._setIsLocked(false);
    });
  }
}

BookItemController.$inject = ['SHARED_EVENTS', 'SharedEventsBusService', 'BookItemService'];

export const BookItemComponent = {
  template,
  controller: BookItemController,
  bindings: {
    item: '<book'
  }
};
