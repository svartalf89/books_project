class BookItemService {
  constructor(SharedRequestService, SharedUrlsService) {
    this.SharedRequestService = SharedRequestService;
    this.SharedUrlsService = SharedUrlsService;
  }

  requestBookUpdate(id, data) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.booksPut(id), {data}));
  }

  requestBookDelete(id) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.booksDelete(id)));
  }
}

BookItemService.$inject = ['SharedRequestService', 'SharedUrlsService'];

export { BookItemService };