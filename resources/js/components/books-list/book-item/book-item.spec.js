describe('BookItemModule', () => {
  let controller;
  let item;
  let $httpBackend;
  let SharedEventsBusService;
  let SHARED_EVENTS;

  beforeEach(() => {
    angular.mock.module('app');
  });

  beforeEach(inject(($componentController, _$httpBackend_, _SharedEventsBusService_, _SHARED_EVENTS_) => {
    $httpBackend = _$httpBackend_;
    SharedEventsBusService = _SharedEventsBusService_;
    SHARED_EVENTS = _SHARED_EVENTS_;

    item = {
      id: 1,
      title: 'Test Title',
      author: 'Test Author'
    };

    controller = $componentController('bookItem', null, {
      item
    });
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined()
  });

  it('should toggle state', () => {
    controller.$onInit();
    expect(controller.state).toBe('display');

    controller.toggleState('edit');
    expect(controller.state).toBe('edit');
    expect(controller.editItem.author).toBe('Test Author');
  });

  it('should update item', () => {
    $httpBackend.expectPUT(/api\/books\/1/, {
      author: 'Test Author 2'
    }).respond(() => [200, 'OK']);

    controller.$onInit();
    controller.toggleState('edit');
    controller.editItem.author = 'Test Author 2';
    controller.updateItem(['author']);
    $httpBackend.flush();

    expect(controller.item.author).toBe('Test Author 2');
    expect(controller.state).toBe('display');
  });

  it('should delete item', () => {
    spyOn(SharedEventsBusService, 'dispatch');
    $httpBackend.expectDELETE(/api\/books\/1/).respond(() => [200, 'OK']);

    controller.deleteItem();
    $httpBackend.flush();

    expect(SharedEventsBusService.dispatch).toHaveBeenCalledWith(SHARED_EVENTS.booksRerender);
  });
});