import angular from 'angular';
import { BookItemService } from './book-item.service';
import { BookItemComponent } from './book-item.component';

export const BookItemModule = angular
  .module('books-list.book-item', [])
  .service('BookItemService', BookItemService)
  .component('bookItem', BookItemComponent)
  .name;