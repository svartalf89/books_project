describe('BooksListModule', () => {
  let controller;
  let $httpBackend;
  let SharedEventsBusService;
  let SHARED_EVENTS;

  beforeEach(() => {
    angular.mock.module('app');
  });

  beforeEach(inject(($componentController, _$httpBackend_, _SharedEventsBusService_, _SHARED_EVENTS_) => {
    $httpBackend = _$httpBackend_;
    SharedEventsBusService = _SharedEventsBusService_;
    SHARED_EVENTS = _SHARED_EVENTS_;

    controller = $componentController('booksList');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined()
  });

  it('should properly init', () => {
    spyOn(SharedEventsBusService, 'subscribe');

    controller.$onInit();

    expect(SharedEventsBusService.subscribe).toHaveBeenCalledTimes(2);
    expect(controller.scrollProcessor).toBeDefined();
  });

  it('should be able to load items', () => {
    const items = [
      {
        id: 1,
        title: 'Test Title 1',
        author: 'Test Author 1'
      },
      {
        id: 2,
        title: 'Test Title 2',
        author: 'Test Author 2'
      }
    ];

    $httpBackend.expectGET(/api\/books/).respond(() => [200, {
      total: 10,
      items
    }]);

    controller.$onInit();
    controller._loadItems(5);

    $httpBackend.flush();

    expect(controller.total).toBe(10);
    expect(controller.items).toEqual(items)
  });

  it('should rerender', () => {
    let testCallback;

    spyOn(SharedEventsBusService, 'subscribe').and.callFake((eventName, callback) => {
      if (eventName === SHARED_EVENTS.booksRerender) {
        testCallback = callback;
      }
    });
    spyOn(controller, '_rerender');
    controller.$onInit();

    testCallback();

    expect(controller._rerender).toHaveBeenCalled();
  });

  it('should rerender with search', () => {
    let testCallback;

    spyOn(SharedEventsBusService, 'subscribe').and.callFake((eventName, callback) => {
      if (eventName === SHARED_EVENTS.booksSearch) {
        testCallback = callback;
      }
    });
    spyOn(controller, '_rerender');
    controller.$onInit();

    testCallback('some search');

    expect(controller.state.search).toBe('some search');
    expect(controller._rerender).toHaveBeenCalled();
  });

  it('should define view methods', () => {
    expect(controller.toggleSortButton).toBeDefined();
    expect(controller.getIconForSort).toBeDefined();
    expect(controller.nullCheck).toBeDefined();
  });
});