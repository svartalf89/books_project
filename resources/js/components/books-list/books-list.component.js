import template from './books-list.html';

class BooksListController {
  constructor(LIMIT, SHARED_EVENTS, SharedEventsBusService, BooksListService) {
    this.SHARED_EVENTS = SHARED_EVENTS;
    this.LIMIT = LIMIT;
    this.SharedEventsBusService = SharedEventsBusService;
    this.BooksListService = BooksListService;
  }

  $onInit() {
    // Shared events subscriptions
    this._unsubscribeBooksSearch = this.SharedEventsBusService.subscribe(
      this.SHARED_EVENTS.booksSearch,
      this._onBooksSearch.bind(this)
    );
    this._unsubscribeBooksRerender = this.SharedEventsBusService.subscribe(
      this.SHARED_EVENTS.booksRerender,
      this._rerender.bind(this)
    );

    // MAterial icons values
    this._orderIcons = {
      asc: 'arrow_drop_up',
      desc: 'arrow_drop_down'
    };

    // Initial state
    this.items = [];
    this.state = {
      search: null,
      sort: null,
      order: null,
      limit: this.LIMIT
    };

    this.scrollProcessor = this._getFreshScrollProcessorCopy();
  }

  $onDestroy() {
    // unsubscribe shared events
    this._unsubscribeBooksSearch && this._unsubscribeBooksSearch();
    this._unsubscribeBooksRerender && this._unsubscribeBooksRerender();
  }

  _onBooksSearch(searchTerm) {
    this.state.search = searchTerm;
    this._rerender();
  }

  // this is needed to force md-virtual-repeat-container rerender
  _getFreshScrollProcessorCopy() {
    const obj = {
      _buffer: 5,
      _expected: 0,
      topIndex: 1,
      getItemAtIndex: (index) => {
        if (index > this.items.length) {
          if (obj._expected < index) {
            obj._expected += this.state.limit;
            this._loadItems(index);
          }

          return null;
        }

        return this.items[index];
      },
      getLength: () => {
        const totalBuffered = this.items.length + obj._buffer;
        return  this.nullCheck(this.total) ? totalBuffered : Math.min(totalBuffered, this.total);
      }
    };

    return obj;
  }

  _rerender() {
    this.total = null;
    this.items = [];
    this.scrollProcessor = this._getFreshScrollProcessorCopy();
  }

  _loadItems(index) {
    const params = _.extend({}, this.state, {start: index});

    return this.BooksListService.requestBooksList(params).then(({items, total}) => {
      this.total = total;
      this.items.splice(index, items.length, ...items);

      return {items, total};
    });
  }

  toggleSortButton(sort) {
    if (this.state.sort === sort) {
      this.state.order = this.state.order === 'desc' ? 'asc' : 'desc';
    } else {
      this.state.sort = sort;
      this.state.order = 'asc';
    }

    this._rerender();
  }

  getIconForSort(sort) {
    return (this.state.sort === sort && !this.nullCheck(this.state.order)) ?
      this._orderIcons[this.state.order] :
      this._orderIcons.asc;
  }

  nullCheck(value) {
    return _.isNull(value) || _.isUndefined(value);
  }
}

BooksListController.$inject = ['LIMIT', 'SHARED_EVENTS', 'SharedEventsBusService', 'BooksListService'];

export const BooksListComponent = {
  template,
  controller: BooksListController
};
