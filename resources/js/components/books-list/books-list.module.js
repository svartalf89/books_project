import angular from 'angular';
import { LIMIT } from './books-list.constants';
import { BooksListService } from './books-list.service';
import { BooksListComponent } from './books-list.component';
import { BookItemModule } from './book-item/book-item.module';

export const BooksListModule = angular
  .module('books-list', [BookItemModule])
  .constant('LIMIT', LIMIT)
  .service('BooksListService', BooksListService)
  .component('booksList', BooksListComponent)
  .name;