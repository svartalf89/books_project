class BooksListService {
  constructor(SharedRequestService, SharedUrlsService) {
    this.SharedRequestService = SharedRequestService;
    this.SharedUrlsService = SharedUrlsService;
  }

  requestBooksList(params) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.booksGet(), {
      params: _.pick(params, _.identity)
    }));
  }
}

BooksListService.$inject = ['SharedRequestService', 'SharedUrlsService'];

export { BooksListService };