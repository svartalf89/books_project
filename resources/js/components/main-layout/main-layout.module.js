import angular from 'angular';
import { Config } from './main-layout.config';
import { MainLayoutComponent } from './main-layout.component';

export const MainLayoutModule = angular
  .module('main-layout', [])
  .config(Config)
  .component('mainLayout', MainLayoutComponent)
  .name;