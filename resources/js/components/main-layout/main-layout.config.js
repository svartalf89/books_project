const Config = ($stateProvider) => {
  $stateProvider.state('main', {
    url: '/main',
    component: 'mainLayout'
  });
};

Config.$inject = ['$stateProvider'];

export { Config };
