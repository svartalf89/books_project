import template from './books-add.html';

class BooksAddController {
  constructor(SHARED_EVENTS, SharedEventsBusService, BooksAddService) {
    this.SHARED_EVENTS = SHARED_EVENTS;
    this.SharedEventsBusService = SharedEventsBusService;
    this.BooksAddService = BooksAddService;
  }

  _resetForm() {
    this.title = '';
    this.author = '';
    this.addBookForm.$setPristine();
    this.addBookForm.$setUntouched()
  }

  addBook() {
    const {author, title} = this;
    this.BooksAddService.requestBooksCreate({author, title}).then(() => {
      this.SharedEventsBusService.dispatch(this.SHARED_EVENTS.booksRerender);
      this._resetForm();
    });
  }
}

BooksAddController.$inject = ['SHARED_EVENTS', 'SharedEventsBusService', 'BooksAddService'];

export const BooksAddComponent = {
  template,
  controller: BooksAddController
};
