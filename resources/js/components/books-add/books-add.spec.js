describe('BooksAddModule', () => {
  let controller;
  let $httpBackend;
  let SharedEventsBusService;
  let SHARED_EVENTS;

  beforeEach(() => {
    angular.mock.module('app');
  });

  beforeEach(inject(($componentController, _$httpBackend_, _SharedEventsBusService_, _SHARED_EVENTS_) => {
    $httpBackend = _$httpBackend_;
    SharedEventsBusService = _SharedEventsBusService_;
    SHARED_EVENTS = _SHARED_EVENTS_;

    controller = $componentController('booksAdd');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined()
  });

  it('should add book', () => {
    spyOn(SharedEventsBusService, 'dispatch');
    spyOn(controller, '_resetForm');

    $httpBackend.expectPOST(/api\/books/, {
      title: 'Some Title',
      author: 'Some Author'
    }).respond(() => [200, 'OK']);

    controller.title = 'Some Title';
    controller.author = 'Some Author';

    controller.addBook();
    $httpBackend.flush();

    expect(SharedEventsBusService.dispatch).toHaveBeenCalledWith(SHARED_EVENTS.booksRerender);
    expect(controller._resetForm).toHaveBeenCalled();
  })
});