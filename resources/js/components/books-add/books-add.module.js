import angular from 'angular';
import { BooksAddService } from './books-add.service';
import { BooksAddComponent } from './books-add.component';

export const BooksAddModule = angular
  .module('books-add', [])
  .service('BooksAddService', BooksAddService)
  .component('booksAdd', BooksAddComponent)
  .name;