class BooksAddService {
  constructor(SharedRequestService, SharedUrlsService) {
    this.SharedRequestService = SharedRequestService;
    this.SharedUrlsService = SharedUrlsService;
  }

  requestBooksCreate(data) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.booksCreate(), {
      data
    }));
  }
}

BooksAddService.$inject = ['SharedRequestService', 'SharedUrlsService'];

export { BooksAddService };