describe('ExportModule', () => {
  let controller;
  let $httpBackend;

  beforeEach(() => {
    angular.mock.module('app');
  });

  beforeEach(inject(($componentController, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;

    controller = $componentController('export');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined()
  });

  it('should send export', () => {
    spyOn(controller, '_requestProgress');
    $httpBackend.expectGET(/api\/export/).respond(() => [200, {
      jobId: 1
    }]);

    controller.columns = ['author', 'title'];
    controller.format = 'xml';
    controller.sendExport();

    $httpBackend.flush();

    expect(controller.state).toBe('progress');
    expect(controller._requestProgress).toHaveBeenCalledWith(1);
  });

  it('should request progress', () => {
    $httpBackend.expectGET(/api\/export\/1/).respond(() => [200, {
      status: 'finished',
      progressPercentage: 100,
      fileId: 'test_1.xml'
    }]);

    controller.$onInit();
    controller._requestProgress(1);

    $httpBackend.flush();

    expect(controller.state).toBe('finished');
    expect(controller.progress.status).toBe('finished');
    expect(controller.progress.percentage).toBe(100);
    expect(controller.progress.downloadLink).toMatch(/test_1\.xml/);
  });
});