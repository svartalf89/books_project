import angular from 'angular';
import { ExportComponent } from './export.component';
import { ExportService } from './export.service';
import { EXPORT_INTERVAL } from './export.constants';

export const ExportModule = angular
  .module('export', [])
  .service('ExportService', ExportService)
  .constant('EXPORT_INTERVAL', EXPORT_INTERVAL)
  .component('export', ExportComponent)
  .name;