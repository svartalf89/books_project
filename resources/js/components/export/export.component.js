import template from './export.html';

class ExportController {
  constructor($timeout, EXPORT_INTERVAL, ExportService) {
    this.$timeout = $timeout;
    this.EXPORT_INTERVAL = EXPORT_INTERVAL;
    this.ExportService = ExportService;
  }

  $onInit() {
    this.format = 'csv';
    this.columns = {
      author: true,
      title: true
    };
    this.progress = {};
    this._setState('form');
  }

  _setState(state) {
    this.state = state;
  }

  _requestProgress(jobId) {
    this.ExportService.requestExportProgress(jobId).then(({status, progressPercentage, fileId}) => {
      _.extend(this.progress, {
        status,
        percentage: progressPercentage
      });

      switch(status) {
        case 'queued':
        case 'executing':
          this.$timeout(() => {
            this._requestProgress(jobId);
          }, this.EXPORT_INTERVAL);
          break;
        case 'finished':
          this.progress.downloadLink = this.ExportService.getExportDownloadLink(fileId);
          this._setState('finished');
          break;
        default:
          this._setState('unknown');
      }
    });
  }

  sendExport() {
    const columns = _.keys(_.pick(this.columns, _.identity));

    this._setState('progress');
    this.ExportService.requestExportStart(this.format, columns).then(({jobId}) => {
      this._requestProgress(jobId)
    });
  }

  reset() {
    this.progress = {};
    this._setState('form');
  }
}

ExportController.$inject = ['$timeout', 'EXPORT_INTERVAL', 'ExportService'];

export const ExportComponent = {
  template,
  controller: ExportController
};
