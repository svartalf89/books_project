class ExportService {
  constructor(SharedRequestService, SharedUrlsService) {
    this.SharedRequestService = SharedRequestService;
    this.SharedUrlsService = SharedUrlsService;
  }

  requestExportStart(format, columns) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.exportStart(), {
      params: {
        format,
        columns: columns.join(',')
      }
    }));
  }

  requestExportProgress(jobId) {
    return this.SharedRequestService.send(_.extend({}, this.SharedUrlsService.exportProgress(jobId)));
  }

  getExportDownloadLink(fileId) {
    return this.SharedUrlsService.exportDownload(fileId).url;
  }
}

ExportService.$inject = ['SharedRequestService', 'SharedUrlsService'];

export { ExportService };