const Config = ($urlRouterProvider, $locationProvider) => {
  $locationProvider.hashPrefix('');
  $urlRouterProvider.otherwise('/main');
};

Config.$inject = ['$urlRouterProvider', '$locationProvider'];

export { Config };
