import angular from 'angular';
import { SharedRequestService } from './shared-request.service';
import { SharedUrlsService } from './shared-urls.service';
import { SHARED_EVENTS } from './shared-events-bus.service';
import { SharedEventsBusService } from './shared-events-bus.service';

export const SharedModule = angular
  .module('app.shared', [])
  .service('SharedRequestService', SharedRequestService)
  .service('SharedUrlsService', SharedUrlsService)
  .constant('SHARED_EVENTS', SHARED_EVENTS)
  .service('SharedEventsBusService', SharedEventsBusService)
  .name;
