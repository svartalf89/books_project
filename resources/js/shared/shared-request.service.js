class SharedRequestService {
  constructor($http, $q, $mdDialog) {
    this.$http = $http;
    this.$q = $q;
    this.$mdDialog = $mdDialog;
  }

  send(...args) {
    return this.$http(...args).then(({data}) => data, (err) => {
      this._errorHandler(err);
      return this.$q.reject(err);
    });
  }

  _errorHandler(err) {
    const alert = this.$mdDialog.alert({
      title: 'Error received',
      textContent: 'Got error from HTTP request. Errors parsing is not implemented yet, sorry :(',
      ok: 'Ok'
    });

    this.$mdDialog.show(alert);
  }
}

SharedRequestService.$inject = ['$http', '$q', '$mdDialog'];

export { SharedRequestService };