const SHARED_EVENTS = {
  booksSearch: 'books-list.search',
  booksRerender: 'books-list.rerender'
};

class SharedEventsBusService {
  constructor($rootScope) {
    this.$rootScope = $rootScope;
  }

  dispatch(eventName, data) {
    this.$rootScope.$emit(eventName, data)
  }

  subscribe(eventName, callback) {
    return this.$rootScope.$on(eventName, (event, data) => {
      callback(data);
    });
  }
}

SharedEventsBusService.$inject = ['$rootScope'];

export { SharedEventsBusService };
export { SHARED_EVENTS }