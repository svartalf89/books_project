class SharedUrlsService {
  constructor() {
    this.baseUrl = '';
  }

  booksGet() {
    return {
      method: 'GET',
      url: this.baseUrl + '/api/books'
    }
  }

  booksPut(id) {
    return {
      method: 'PUT',
      url: this.baseUrl + '/api/books/' + id
    }
  }

  booksDelete(id) {
    return {
      method: 'DELETE',
      url: this.baseUrl + '/api/books/' + id
    }
  }

  booksCreate() {
    return {
      method: 'POST',
      url: this.baseUrl + '/api/books'
    }
  }

  exportStart() {
    return {
      method: 'GET',
      url: this.baseUrl + '/api/export'
    }
  }

  exportProgress(jobId) {
    return {
      method: 'GET',
      url: this.baseUrl + '/api/export/' + jobId
    }
  }

  exportDownload(fileId) {
    return {
      url: this.baseUrl + '/api/export/download/' + fileId
    }
  }
}

export { SharedUrlsService };