import underscore from 'underscore';
window._ = underscore;

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngMaterial from 'angular-material';
import ngAnimate from 'angular-animate';
import ngMessages from 'angular-messages';

import { SharedModule } from './shared/shared.module';
import { ComponentsModule } from './components/components.module';
import { Config } from './app.config'
import { AppComponent } from './app.component';

export const AppModule = angular
  .module('app', [
    uiRouter,
    ngMaterial,
    ngAnimate,
    ngMessages,
    SharedModule,
    ComponentsModule
  ])
  .config(Config)
  .component('app', AppComponent)
  .name;
