<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Books Project</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src={{ @mix('/js/manifest.js') }}></script>
        <script src={{ @mix('/js/vendor.js') }}></script>
        <script src={{ @mix('/js/app.js') }}></script>

        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <app></app>
    </body>
</html>
