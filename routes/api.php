<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// List RESTful stuff
Route::get('books', 'BookController@index');
Route::post('books', 'BookController@create');
Route::put('books/{book}', 'BookController@update');
Route::delete('books/{book}', 'BookController@delete');

// Export stuff
Route::get('export', 'ExportController@books');
Route::get('export/{jobId}', 'ExportController@getJob')->where('jobId', '[0-9]+');
Route::get('export/download/{fileId}', 'ExportController@download');
