let webpackConf = require('laravel-mix/setup/webpack.config');
delete webpackConf.entry;

module.exports = function(config) {
  config.set({
    basePath: './resources/js/',
    frameworks: ['jasmine'],
    files: ['app.specs.js'],
    preprocessors: {
      'app.specs.js': ['webpack'],
      '**/*.spec.js': ['webpack']
    },
    webpack: webpackConf,
    webpackMiddleware: {
      noInfo: true,
      stats: 'errors-only'
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  })
};