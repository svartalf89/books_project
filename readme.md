## Books Project

This is simple app developed with Laravel 5.7, MySql 14.14 and AngularJS 1.7

Books list with the following functionality:

- Add a book to the list.
- Delete a book from the list.
- Change an authors name
- Sort by title or author
- Search for a book by title or author
- Export the the following in CSV and XML
    - A list with Title and Author
    - A list with only Titles
    - A list with only Authors

## Environment Setup
Download and install Homestead Vagrant box - https://laravel.com/docs/5.7/homestead

Sources of this project should be placed inside vagrant box, into `/home/vagrant/code/books_project`

Install composer packages - `composer install`

Migrate DB tables - `php artisan migrate`

Create book DB records - `php artisan db:seed`

Install npm packages - `npm install`

## Build dev
Build FE - `npm run dev`

Laravel Mix (https://github.com/JeffreyWay/laravel-mix) is used for the front-end build.

PHP will work after `composer install`

## Build Production
Build optimised composer loader:

`composer install --optimize-autoloader --no-dev`

Build FE package:

`npm run prod`

## Export
Export is implemented as async queueable job. 

So to make it work properly, ***need to start queue worker***:

`cd /home/vagrant/code/books_project`

`php artisan queue:work`

Export jobs statuses are stored in database, there is special artisan command to clear expired ones (by default > 30 minutes):

`php artisan cleanup:export` - it can be assigned to a scheduler, like Cron etc. 

Also added corresponding command to Laravel's Console Kernel, could be run like this:

`* * * * * php /home/vagrant/code/books_project/artisan schedule:run 1>> /dev/null 2>&1`

## Unit tests
To run Javascript unit tests - Karma seems not working very well with Laravel Mix export method, so I added separate command:

`npm run test`

To run PHP unit tests:

`phpunit`

---

